﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp34
{
    class Program
    {
        static Random r = new Random();

        static void Main(string[] args)
        {
            DateTime start = DateTime.Now;

            // 1. variant - ToList teeb kõik asjad järjest
            // 2. variant - ToParallel teeb kõrvuti, max processorite arv "threadi"

            //Enumerable.Range(1, 16)
            //.ToList().ForEach(x => Console.WriteLine(Tegevus(x)));
            //    .AsParallel().ForAll(x => Console.WriteLine(Tegevus(x)));

            // 3. variant - Selectiga, teeb KÕIK paralleelselt
            /*
            var q = Enumerable.Range(1, 16) // ei ole piirangut, palju threade - mälu piir ja vist 2G (int) piir
                    .Select(x => TegevusA(x)).ToArray();
            Task.WaitAll(q);
            foreach (var x in q) Console.WriteLine(x.Result);
            //*/

            Console.WriteLine($"Taskid kokku kestsid {(DateTime.Now - start).TotalSeconds}");
        }

        static string Tegevus(int nr)
        {
            Console.WriteLine($"Task {nr} alustas");
            DateTime start = DateTime.Now;
            // Task.Delay(1000 * (r.Next() % 10) + 1000 ).Wait(); 
            Thread.Sleep(100 * (r.Next() % 10) + 1000);
            return ($"Task {nr} kestis {(DateTime.Now - start).TotalSeconds}");
        }
        static async Task<string> TegevusA(int nr)
        {
            Console.WriteLine($"Task {nr} alustas");
            DateTime start = DateTime.Now;
            await Task.Delay(100 * (r.Next() % 10) + 1000 ); 
            // Thread.Sleep(100 * (r.Next() % 10) + 1000);
            return ($"Task {nr} kestis {(DateTime.Now - start).TotalSeconds}");
        }


    }
}
