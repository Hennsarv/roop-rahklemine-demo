﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace ConsoleApp35
{
    class Program
    {
        static void Main(string[] args)
        {
            AsyncMain(args).Wait();
        }

        static async Task AsyncMain(string[]  args)
        {
            Dictionary<string, string> urlid = new Dictionary<string, string>()
            {
                { "Lõvi", @"https://upload.wikimedia.org/wikipedia/commons/c/cc/Lion_in_Kenya.jpg" },
                { "Krokodill",@"https://upload.wikimedia.org/wikipedia/commons/b/bd/Nile_crocodile_head.jpg" },
                { "Jänes", @"https://upload.wikimedia.org/wikipedia/commons/4/46/Feldhase_Schiermonnikoog.JPG" },
                { "Tiiger", @"https://upload.wikimedia.org/wikipedia/commons/1/17/Tiger_in_Ranthambhore.jpg" },
                { "Siil", @"https://upload.wikimedia.org/wikipedia/commons/e/e1/Erinaceus_europaeus_LC0119.jpg" },
                { "Konn", @"https://upload.wikimedia.org/wikipedia/commons/8/8b/Australia_green_tree_frog_%28Litoria_caerulea%29_crop.jpg" },
                { "Toonekurg", @"https://upload.wikimedia.org/wikipedia/commons/d/d4/Ciconia_ciconia_%28aka%29.jpg"},
                { "Põder", @"https://upload.wikimedia.org/wikipedia/commons/a/a6/Elch_3_db.jpg" }
            };

            Dictionary<string, Task<double>> ootamised = new Dictionary<string, Task<double>>();
            
            DateTime start = DateTime.Now;
            //* Variant 1
            foreach (var x in urlid)
            {
                
                var t = KopiVeebist(x.Value, @"..\..\Pildid\" + x.Key + ".jpg");
                ootamised.Add(x.Key, t);
            }
            Task.WaitAll(ootamised.Values.ToArray());

            double summa = 0;

            foreach(var x in ootamised)
            {
                double d = x.Value.Result;
                summa += d;
                Console.WriteLine($"{x.Key} laadimiseks kulus {d} sekundit");
            }
            //*/


            Console.WriteLine($"Kokku kulus {(DateTime.Now - start).TotalSeconds} sekundit, eraldi {summa} sekundit" );

        }

        static async Task<double> KopiVeebist(string url, string path)
        {
            DateTime start = DateTime.Now;
            using (Stream sr = HttpWebRequest.CreateHttp(url).GetResponse().GetResponseStream())
            {
                using (FileStream fs = File.Open(path, FileMode.Create))
                {
                    await sr.CopyToAsync(fs);
                }
            }

                return (DateTime.Now - start).TotalSeconds;
        }

    }
}
